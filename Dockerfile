### --- Build Phase --- ###
FROM ruby:2.7.0-alpine AS BUILD

ARG ROOT_PATH=/root
ARG DEV_PACKAGES="build-base tzdata postgresql-dev nodejs yarn bash"

# These are used only for Rails to precompile assets
ENV SECRET_KEY_BASE=21fb7205d746fd4e093291cc8fc9c87d
ENV RAILS_MASTER_KEY=ae25e2db6bbb94865c5109ea0ccf6b88b28cd2fee3c34174d1a41c7ddf039247168f79a6c90d4d2169d37ae65406080ddc7e47a8dd52b41027ed06df00430ecf
ENV RAILS_ENV=production
ENV NODE_ENV=production

WORKDIR $ROOT_PATH

COPY . .
# Install packages needed for building GEMs
RUN apk add --no-cache --update $DEV_PACKAGES \
  # Updade bundler
  && gem update bundler \
  # Configure bundler
  && bundle config set without "development test" \
  && bundle config set --local path "vendor/bundle" \
  && bundle install \
  # Install JS processor
  && yarn install \
  # Build JS files
  && bin/rails assets:precompile \
  # Cleanup files that are no longer needed
  && bash production_cleanup.sh

### xxx Build Phase xxx ###

### --- Deploy Phase --- ###
FROM ruby:2.7.0-alpine

ENV RAILS_ENV=production
ENV RAILS_SERVE_STATIC_FILES=true
ARG ROOT_PATH=/root
ARG PACKAGES="tzdata postgresql-client nodejs bash wkhtmltopdf xvfb-run msttcorefonts-installer fontconfig"

WORKDIR $ROOT_PATH
EXPOSE 3000

COPY --from=BUILD $ROOT_PATH $ROOT_PATH
RUN apk upgrade --update --no-cache \
  && apk add --update --no-cache $PACKAGES \
  && cp /usr/share/zoneinfo/Europe/Prague /etc/localtime \
  && bundle config set without "development test" \
  && bundle config set --local path "vendor/bundle" \
  && update-ms-fonts \
  && fc-cache -f

CMD bin/rails db:migrate && bin/rails s -b 0.0.0.0
### xxx Deploy Phase xxx ###
