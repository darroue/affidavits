class CreateAffidavits < ActiveRecord::Migration[6.1]
  def change
    create_table :affidavits do |t|
      t.references :user, null: false, foreign_key: true
      t.references :who, null: false
      t.references :where, null: false
      t.string :reason
      t.string :note

      t.timestamps
    end
  end
end
