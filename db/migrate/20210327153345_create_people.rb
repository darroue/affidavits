class CreatePeople < ActiveRecord::Migration[6.1]
  def change
    create_table :people do |t|
      t.string :first_name
      t.string :last_name
      t.string :nickname
      t.string :id_number
      t.string :phone_number
      t.string :street
      t.string :houser_number
      t.string :city
      t.string :preferred_reason
      t.string :preferred_note
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
