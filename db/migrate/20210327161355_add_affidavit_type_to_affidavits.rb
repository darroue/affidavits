class AddAffidavitTypeToAffidavits < ActiveRecord::Migration[6.1]
  def change
    add_column :affidavits, :affidavit_type, :string
  end
end
