class AddPreferredAffidavitTypeToPeople < ActiveRecord::Migration[6.1]
  def change
    add_column :people, :preferred_affidavit_type, :string
  end
end
