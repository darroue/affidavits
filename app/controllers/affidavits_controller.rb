class AffidavitsController < ApplicationController
  before_action :set_instance_variables, only: :show

  def show
    respond_to do |format|
      template = "affidavits/#{@affidavit_type}.html.slim"

      format.html do
        render template: template
      end

      format.pdf do
        render template: template, pdf: [@who.nickname, @where.nickname].join('_') + '.pdf'
      end
    end
  end

  def create
    %i(who where).each do |key|
      affidavit_params[key] = current_user.people.find_by(id: affidavit_params[key])
    end

    @affidavit = current_user.affidavits.new(affidavit_params)

    respond_to do |format|
      if @affidavit.save
        format.html { redirect_to @affidavit, notice: "Affidavit was successfully created." }
        format.json { render :show, status: :created, location: @affidavit }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @affidavit.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @affidavit = current_user.affidavits.find_by(id: params[:id])

    respond_to do |format|
      if @affidavit.destroy
        flash[:notice] = "Affidavit was successfully destroyed."
      else
        flash[:error] = "Error while destroing affidavit."
      end

      format.html do
        redirect_to people_path
      end
    end
  end

  def show_or_create
    @user = current_user
    @people = @user.people
    @who = @people.find_by(nickname: params[:who_nickname])
    @where = @people.find_by(nickname: params[:where_nickname])

    return head :not_found unless @who && @where

    @affidavit_type = params[:affidavit_type] || @who&.preferred_affidavit_type || 'region'
    @reason = params[:reason] || @who&.preferred_reason || 'other_person'
    @note = params[:note] || @who&.preferred_note || ''

    @date = if params[:date].present?
      Date.parse(params[:date]) rescue nil
    end

    query = @user.affidavits.where(affidavit_show_or_create_params)
    @affidavit = if @date
      query.where(created_at: @date.to_time..(@date.next - 1.second))
    else
      query.where("created_at >= ?", Date.today)
    end.first

    unless @affidavit.present?
      @affidavit = @user.affidavits.create!(**affidavit_show_or_create_params)
      @affidavit.update(created_at: Time.now - 1.hour )
    end

    show
  end

  private

  def affidavit_attributes
    %i(
      who
      where
      affidavit_type
      reason
      note
    )
  end

  def affidavit_params
    @affidavit_params ||= params.require(:affidavit).permit(*affidavit_attributes)
  end

  def affidavit_show_or_create_params
    affidavit_attributes.each_with_object({}) do |key, hash|
      hash[key] = instance_variable_get("@#{key}")
    end
  end

  def set_instance_variables
    unless @affidavit
      @affidavit = current_user.affidavits.find_by(id: params[:id])

      if @affidavit
        %i(
          affidavit_type
          who
          where
          reason
          note
        ).each do |var|
          instance_variable_set("@#{var}", @affidavit.send(var))
        end
      else
        flash[:error] = "Affidavit not found!"
        redirect_to :root
      end
    end
  end
end
