# == Schema Information
#
# Table name: affidavits
#
#  id             :bigint           not null, primary key
#  affidavit_type :string
#  note           :string
#  reason         :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :bigint           not null
#  where_id       :bigint           not null
#  who_id         :bigint           not null
#
# Indexes
#
#  index_affidavits_on_user_id   (user_id)
#  index_affidavits_on_where_id  (where_id)
#  index_affidavits_on_who_id    (who_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Affidavit < ApplicationRecord
  belongs_to :user
  belongs_to :who, class_name: 'Person'
  belongs_to :where, class_name: 'Person'

  def rounded_time
    Time.at(created_at.to_i - (created_at.to_i % 15.minutes))
  end
end
