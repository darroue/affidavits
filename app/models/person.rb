# == Schema Information
#
# Table name: people
#
#  id                       :bigint           not null, primary key
#  city                     :string
#  first_name               :string
#  house_number             :string
#  id_number                :string
#  last_name                :string
#  nickname                 :string
#  phone_number             :string
#  preferred_affidavit_type :string
#  preferred_note           :string
#  preferred_reason         :string
#  street                   :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  user_id                  :bigint           not null
#
# Indexes
#
#  index_people_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Person < ApplicationRecord
  belongs_to :user
  has_one_attached :signature

  before_save :unaccent_nickname

  validates *%i(
    first_name
    last_name
    nickname
  ), presence: true

  def unaccent_nickname
    self.nickname = I18n.transliterate(nickname).downcase
  end

  def display_name
    [first_name, last_name].join(' ')
  end

  def place
    "#{street} #{house_number}, #{city}"
  end
end
