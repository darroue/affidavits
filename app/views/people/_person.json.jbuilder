json.extract! person, :id, :first_name, :last_name, :nickname, :id_number, :phone_number, :street, :house_number, :city, :preferred_reason, :preferred_note, :user_id, :created_at, :updated_at
json.url person_url(person, format: :json)
