Rails.application.routes.draw do
  root action: :index, controller: :people
  resources :people
  devise_for :users
  
  get '/affidavits/:who_nickname/:where_nickname', controller: :affidavits, action: :show_or_create, as: :show_or_create_affidavits
  resources :affidavits, only: %i(show create destroy)
end
